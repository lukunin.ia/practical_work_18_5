#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

/*���������*/
//players/player - ������
//players_count - ���������� �������
//score - ����
//d_arr - ������������ ������ (d_arr)
//size - ������ �������

/*����������� ���������*/
class Player
{
public:// ������ ������ ��������� �����
    char Name[70]; //��� ������
    int Scores; //���� ������
};

/*������� ����������*/
void Sort(Player* M, int size) //������� ���������� ������� "���������� ���������"
{
    for (int i = 1; i < size; i++)
    {
        int j = i - 1;
        while (j >= 0 && M[j].Scores <= M[j + 1].Scores)
        {
            swap(M[j].Scores, M[j + 1].Scores);
            cout << "����������" << endl;  //(����� ����� "����������" ��� ��������, ���-�� ������� ���������� ���������� �������� ����������)
            j--;
        }
    }
}
/*����� ������� ����������*/

/*��������������� �������, ���� ������������ ������ ��� ������ �������*/
int get_max_caption(Player* M, int N) {
    int maximum = strlen(M[0].Name);

    int temp;
    for (int i = 1; i < N; i++) {
        temp = strlen(M[i].Name);
        if (temp > maximum) maximum = temp;
    }
    return maximum;
}
/*����� ��������������� �������*/

/*������� ������������ ����� ������ �� �����*/
void ShowData(Player* M, int N)
{
    cout << "\n";
    cout << "������:\n";

    struct ColumnWidth //��������������� ��������� ������� ������������ ������������� ������
    {
        int Name;
    } ColumnWidth;
    int temp; //���������� ��� �������� ���������� ������

    /*����ר� ������ ������ ��������*/
    ColumnWidth.Name = strlen("�����") + 1;
    temp = get_max_caption(M, N) + 1;
    if (ColumnWidth.Name < temp) ColumnWidth.Name = temp;
    /*����� ���ר��*/

    /*������-���������*/
    cout << "�����" << '|';
    cout << "�����" << setw(ColumnWidth.Name - strlen("�����")) << '|';
    cout << setw(10) << "����" << '|';
    /*����� �����-����������*/
    cout << '\n';

    /*������� ������ �������� ���������*/
    for (int i = 0; i < N; i++) {
        cout << i + 1 << setw(5) << '|' << M[i].Name << setw(ColumnWidth.Name - strlen(M[i].Name)) << '|';

        cout.width(10);
        (cout << setprecision(2) << fixed << M[i].Scores << '|').width(10);

        cout << '\n';
    }
    /*����� ����������� ������*/

    cout << '\n';

}

/*������� ������� ������ � ���������*/
void GetData(Player* M, int N)
{
    for (int i = 0; i < N; i++)
    {
        cout << "\n";
        cout << "������� ��� " << (i + 1) << " ������: ";
        cin.getline(M[i].Name, sizeof(M[i].Name));

        cout << "������� ���� " << (i + 1) << " ������: ";
        cin >> M[i].Scores;
        cin.ignore();
    }
}

/*������� �������*/
int main()
{
    setlocale(LC_ALL, "Rus"); //������� ����������� �������

    //������ ���������� �������
    int size;
    cout << "������� ���������� �������: ";
    (cin >> size).ignore(); //���� ���������� ������� (������ �������)

    cout << "\n";
    cout << "==============================================" << endl;

    cout << "������� ����� " << size << " �������: " << endl;

    Player* M = new Player[size];
    GetData(M, size); //���� ������ � ������ ��������

    cout << "\n";
    cout << "==============================================" << endl;

    ShowData(M, size); //����� ������� �������� �� ����� �� ���������� (��� ��������)

    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
    Sort(M, size); //����� ������� ���������� �������
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

    ShowData(M, size); //����� ������ ����� ����������

    delete[]M; //������� ������������ ������

    return 0;
}
